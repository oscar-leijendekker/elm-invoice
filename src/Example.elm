module Example exposing (main)

import Invoice exposing (..)

--main : Html a
main =
  let
    invoiceTextData : InvoiceTextData
    invoiceTextData =
      { leftHeaderRegular = "Dr"
      , leftHeaderItalic = "Bearhands"
      , rightHeader = invoiceNrText 17
      , rightSubHeader = "1 April 2012"
      , leftEntries =
        [ [ ( "Client", "Sample Client" )
          , ( "", "123 Street Address" )
          , ( "", "City, State 55555" )
          ]
        , [ ( "Project", "#123.456" ) ]
        , [ ( "Contact", "Jane Smith" )
          , ("☎", "+1 (555) 555-5555")]
        ]
      , rightEntries =
        [ [ ( "Payable to", "Your Name" )
          , ( "", "1234 Sycamore Ave" )
          , ( "", "Blacksburg, VA 24060" )
          , ( "☎", "+1 (555) 555-5555" )
          , ( "✉", "you@example.com" )
          ]
        ]
      , currency = "€"
      , currencyPosition = BeforeNumber
      , sumTotal = "1337.69"
      , dueDate = "due April 26, 2015"
      , tableHeaders = ["Date", "Project", "Description", "Hours", "Rate", "Amount"]
      , tableEntries =  [ [ "2015/4/11", "Invoice", "Discussed requirements with client", "2", "€50", "€100.00"]
                        , [ "2015/4/11", "Invoice", "Wrote Invoice in LaTeX", "1.25", "€50", "€62.50" ]
                        , [ "2015/4/11", "Invoice", "Packaged incoive in .cls file", "0.5", "€50", "€25.00" ]
                        ]
      , trailingValues =  [ ("Total", "€187.50")
                          , ("Paid", "€50.00")
                          , ("Balance due", "€137.50")
                          ]
      }
  in
    a4paper <| invoiceLayout invoiceTextData

invoiceNrText : Int -> String
invoiceNrText n = "INVOICE #" ++ String.fromInt n
